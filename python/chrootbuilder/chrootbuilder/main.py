#!/usr/bin/env python3

# Copyright (C) 2020 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
    Tool to export a docker container/image's filesystem and ensure
    compatability with buildbox-run-userchroot.
"""

# Standard Library Imports
import argparse
import os
import tarfile

# External Dependencies
import docker


def parse_args() -> argparse.Namespace:
    """ Parse and return command-line arguments. """
    parser = argparse.ArgumentParser(
        prog="chrootbuilder",
        description="Build chroot images from docker images/containers",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument(
        '-c',
        '--container',
        dest='container',
        action='store_true',
        help='export docker container as chroot (defaults to image)')

    parser.add_argument(
        '-p',
        '--preserve',
        dest='preserve',
        action='store_true',
        help='do not modify the chroot image to be compatible with buildbox-run-userchroot')

    parser.add_argument(
        '-e',
        '--extract',
        dest='extract',
        action='store_true',
        help='write filesystem directory to location instead of tarfile')

    parser.add_argument(
        'name',
        help='The name/id of the docker image (container if --container is specified)')

    parser.add_argument(
        'location',
        help='location to place chroot image (root directory)')

    return parser.parse_args()


def tar_from_image(location: str, name: str, preserve: bool = False) -> None:
    """ Get tar file from docker image name/id. """
    client = docker.from_env()
    container = client.containers.create(name)
    if not preserve:
        # most efficient way to modify container's filesystem
        # overwrite /dev with file, and make it a directory again
        dev_override = tarfile.TarInfo("/dev")
        dev_override.type = tarfile.REGTYPE
        container.put_archive('/', dev_override.tobuf())
        dev_override.type = tarfile.DIRTYPE
        container.put_archive('/', dev_override.tobuf())

    with open(location, 'wb') as chroot_tar:
        bits = container.export()
        for chunk in bits:
            chroot_tar.write(chunk)
    container.remove(v=True)


def tar_from_container(location: str, name: str, preserve: bool = False) -> None:
    """ Get tar file from docker container name/id. """
    client = docker.from_env()
    container = client.containers.get(name)
    with open(location, 'wb') as chroot_tar:
        if not preserve:
            # can't modify filesystem, so run tar directly
            # will only work if gnu tar is on container
            command = "tar --exclude='/dev' --one-file-system -cf - /"
            stream = container.exec_run(command, stream=True, stderr=False)
            bits = stream.output
        else:
            bits = container.export()

        for chunk in bits:
            chroot_tar.write(chunk)


def add_required_dirs(location: str) -> None:
    """ Add directories required by buildbox-run-userchroot. """
    with tarfile.open(location, 'a:') as tar:
        for name in ['/tmp', '/dev']:
            tmp = tarfile.TarInfo(name)
            tmp.type = tarfile.DIRTYPE
            tar.addfile(tmp)


def extract_tar(location: str) -> None:
    """ Extract tarred file into directory. """
    extract_dir = location[:location.rfind("/")]
    with tarfile.open(location, 'r') as tar:
        tar.extractall(extract_dir)

    os.remove(location)


def verify_path(location: str, extract: bool = False) -> None:
    """ Verify that the path has been passed in correctly. """
    if extract:
        if not os.path.exists(location):
            os.mkdir(location)
        elif not os.path.isdir(location):
            raise NotADirectoryError(f"{location} exists and is not a directory")
    else:
        parent = location[:location.rfind("/")]
        if not os.path.isdir(parent):
            raise NotADirectoryError(f"{parent} does not exist or is not a directory")
        if os.path.exists(location):
            raise FileExistsError(f"{location} already exists")


def normalize_path(location: str) -> str:
    """ Expand home directory and normalize path. """
    location = os.path.expanduser(location)
    return os.path.abspath(location)


def main() -> None:
    """ Entry point. """
    args = parse_args()
    location = normalize_path(args.location)
    verify_path(location, args.extract)
    if args.extract:
        location += "/docker.tar"
    if args.container:
        tar_from_container(location, args.name, args.preserve)
    else:
        tar_from_image(location, args.name, args.preserve)
    if not args.preserve:
        add_required_dirs(location)
    if args.extract:
        extract_tar(location)


if __name__ == "__main__":
    main()
