#!/usr/bin/env python3

# Copyright (C) 2020 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from os.path import expanduser
import pathlib
from unittest import mock
import pytest

from chrootbuilder import (
    tar_from_image,
    tar_from_container,
    add_required_dirs,
    extract_tar,
    verify_path,
    normalize_path
)


def assert_exception(exception, func, *args):
    try:
        func(*args)
    except exception:
        return
    assert False


@mock.patch('docker.from_env', autospec=True)
@mock.patch('builtins.open', mock.mock_open(read_data="data"))
def test_tar_from_image(docker_mock):
    container_mock = mock.Mock()
    container_mock.export.return_value = ['bit1', 'bit2']
    docker_mock.return_value.containers.create.return_value = container_mock
    tar_from_image("test", "test", False)
    docker_mock.assert_called()
    container_mock.put_archive.assert_called()
    container_mock.remove.assert_called()


@mock.patch('docker.from_env', autospec=True)
@mock.patch('builtins.open', mock.mock_open(read_data="data"))
def test_tar_from_image_preserve(docker_mock):
    container_mock = mock.Mock()
    container_mock.export.return_value = ['bit1', 'bit2']
    docker_mock.return_value.containers.create.return_value = container_mock
    tar_from_image("test", "test", True)
    docker_mock.assert_called()
    container_mock.put_archive.assert_not_called()
    container_mock.remove.assert_called()


@mock.patch('docker.from_env', autospec=True)
@mock.patch('builtins.open', mock.mock_open(read_data="data"))
def test_tar_from_container(docker_mock):
    container_mock = mock.Mock()
    stream_mock = mock.Mock()
    stream_mock.output = ['bit1', 'bit2']
    container_mock.exec_run.return_value = stream_mock
    docker_mock.return_value.containers.get.return_value = container_mock
    tar_from_container("test", "test", False)
    docker_mock.assert_called()
    container_mock.exec_run.assert_called()
    container_mock.remove.assert_not_called()


@mock.patch('docker.from_env', autospec=True)
@mock.patch('builtins.open', mock.mock_open(read_data="data"))
def test_tar_from_container_preserve(docker_mock):
    container_mock = mock.Mock()
    container_mock.export.return_value = ['bit1', 'bit2']
    docker_mock.return_value.containers.get.return_value = container_mock
    tar_from_container("test", "test", True)
    docker_mock.assert_called()
    container_mock.exec_run.assert_not_called()
    container_mock.remove.assert_not_called()


@mock.patch('tarfile.open', autospec=True)
def test_add_required_dirs(open_mock):
    tar_mock = mock.Mock()
    open_mock.return_value.__enter__.return_value.addfile = tar_mock
    add_required_dirs("test")
    tar_mock.assert_called()
    assert tar_mock.call_count == 2


@mock.patch('os.remove')
@mock.patch('tarfile.open', autospec=True)
def test_extract_tar(open_mock, remove_mock):
    tar_mock = mock.Mock()
    open_mock.return_value.__enter__.return_value.extractall = tar_mock
    extract_tar("test")
    tar_mock.assert_called_once()
    remove_mock.assert_called_once()


def test_verify_path_not_exist():
    cwd = pathlib.Path().absolute()
    fake_path = str(cwd)+"/this_path_should_not_exist_42"
    verify_path(fake_path, False)


def test_verify_path_exist():
    this_file = str(pathlib.Path(__file__).absolute())
    assert_exception(FileExistsError, verify_path, this_file, False)


def test_verify_parent_not_exist():
    fake_parent = "/directory_should_not_exist/test.tar"
    assert_exception(NotADirectoryError, verify_path, fake_parent, False)


@mock.patch('os.mkdir')
def test_verify_path_not_exist_extract(mkdir_mock):
    fake_path = "/fake_path_should_not_exist"
    verify_path(fake_path, True)
    mkdir_mock.assert_called_once_with(fake_path)


@mock.patch('os.mkdir')
def test_verify_path_exist_extract(mkdir_mock):
    cwd = str(pathlib.Path().absolute())
    verify_path(cwd, True)
    mkdir_mock.assert_not_called()


@mock.patch('os.mkdir')
def test_verify_path_not_dir_extract(mkdir_mock):
    this_file = str(pathlib.Path(__file__).absolute())
    assert_exception(NotADirectoryError, verify_path, this_file, True)
    mkdir_mock.assert_not_called()


def test_normalize_path():
    test_path = "~/home/../test//fake"
    expected = expanduser("~/test/fake")
    assert normalize_path(test_path) == expected


def test_normalized_path_absolute():
    test_path = "/home/../test//fake"
    expected = "/test/fake"
    assert normalize_path(test_path) == expected


def test_normalized_path_normalized():
    test_path = "/test//fake"
    expected = "/test/fake"
    assert normalize_path(test_path) == expected
