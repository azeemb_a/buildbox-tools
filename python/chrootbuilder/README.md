# chrootbuilder

## Usage

Usage:
```
chrootbuilder [--help] [--container] [--preserve] [--extract] name location
 ```
Build chroot images (tarball) from docker images/containers

Name can refer to a docker image's name or id. If you wish to specify a container's name or id,
add the --container option. Location represents where to write the tarred filesystem to on disk.
Note that this location cannot exist. Likewise if --extract is specified, the location must be a directory, 
or it cannot exist.

## Example Usages:

Export filesystem in docker image docker_name to chroot.tar in current working directory:
```
chrootbuilder docker_name chroot.tar
```

Export and unpack filesystem in docker image docker_name to chroot directory in current working directory:
```
chrootbuilder -e docker_name chroot
```

Export docker container my_container to /location/to/extraxt.tar:
```
chrootbuilder -c my_container /location/to/extraxt.tar
```

Do not modify the filesystem image at all when exporting (modified by default to be compatabile with buildbox-run-userchroot):
```
chrootbuilder -p docker_name chroot.tar
```

## Installing:
```
pip install .
```
