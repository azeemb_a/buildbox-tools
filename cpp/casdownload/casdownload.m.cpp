/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcommon_client.h>
#include <buildboxcommon_exception.h>
#include <buildboxcommon_fileutils.h>

#include <chrono>
#include <fcntl.h>
#include <iomanip>
#include <iostream>
#include <libgen.h> // dirname
#include <memory>
#include <regex>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

using buildboxcommon::Digest;

void printUsage(const char *program_name)
{
    std::cout
        << "Usage: " << program_name
        << " --instance=INSTANCE --cas-server=ADDRESS "
           "--[root|action|file]-digest=DIGEST --destination-dir=DIRECTORY\n"
        << "Download the specified DIGEST into specified destination "
           "DIRECTORY from the instance INSTANCE of the CAS configured at "
           "ADDRESS.\n"
        << "\n"
        << "DIGEST can be one of:\n"
        << "\t--root-digest=DIGEST\tDownload a tree by its root node ID\n"
        << "\t--action-digest=DIGEST\tDownload the stderr, stdout, output "
           "files and directories of the specified action via the "
           "ActionCache.\n"
        << "\t                      \tThe ActionCache must be hosted in the "
           "same server as the CAS server at ADDRESS.\n"
        << "\t--file-digest=DIGEST\tDownload a blob by its content to "
           "DESTINATION/blob.\n\n"
        << "Example usage:\n"
        << program_name
        << " --instance=dev --cas-server=http://localhost:50051 "
           "--root-digest=deadbeef/412 --destination-dir=output\n"
        << std::endl;
}

bool actionResultFromAction(const Digest &action,
                            const buildboxcommon::ConnectionOptions &options,
                            buildboxcommon::Client *client,
                            buildboxcommon::ActionResult *actionResult)
{
    std::unique_ptr<buildboxcommon::ActionCache::Stub> actionCache =
        buildboxcommon::ActionCache::NewStub(options.createChannel());

    buildboxcommon::GetActionResultRequest actionRequest;
    actionRequest.set_instance_name(client->instanceName());

    actionRequest.set_inline_stdout(false);
    actionRequest.set_inline_stderr(false);
    *actionRequest.mutable_action_digest() = action;

    grpc::ClientContext context;
    const grpc::Status status =
        actionCache->GetActionResult(&context, actionRequest, actionResult);

    if (!status.ok()) {
        BUILDBOXCOMMON_THROW_EXCEPTION(
            std::runtime_error, "Action cache returned error "
                                    << std::to_string(status.error_code())
                                    << ": \""
                                    << status.error_message() + "\"");
    }

    return true;
}

std::string calculateDirname(const std::string &path)
{
    // some dirname implementations modify the input so we have to copy
    std::vector<char> cpath(path.cbegin(), path.cend());
    cpath.push_back('\0');
    return std::string(dirname(&cpath[0]));
}
void createDirectoryForPath(const std::string &path)
{
    const std::string dir = calculateDirname(path);
    buildboxcommon::FileUtils::createDirectory(dir.c_str());
}

void writeExitCode(const std::string &file, int code)
{
    std::ostringstream rc;
    rc << code << "\n";
    buildboxcommon::FileUtils::writeFileAtomically(file, rc.str());
    std::cout << "Wrote rc=" << code << " to " << file << std::endl;
}

void writeActionResultToDisk(const buildboxcommon::ActionResult &actionResult,
                             const std::string &destinationDirectory,
                             const std::string &metadataDir,
                             buildboxcommon::Client *remoteCasClient)

{
    buildboxcommon::Client::OutputMap blobs;
    std::vector<Digest> digests;
    for (const auto &dirIter : actionResult.output_directories()) {
        const std::string fullPath =
            destinationDirectory + "/" + dirIter.path();
        buildboxcommon::FileUtils::createDirectory(fullPath.c_str());
    }

    for (const auto &fileIter : actionResult.output_files()) {
        digests.push_back(fileIter.digest());
        const std::string fullPath =
            destinationDirectory + "/" + fileIter.path();
        blobs.emplace(fileIter.digest().hash(),
                      buildboxcommon::Client::OutputMap::mapped_type(
                          fullPath, fileIter.is_executable()));

        if (fileIter.path().find("/") != std::string::npos) {
            createDirectoryForPath(fullPath);
        }
    }
    const std::string stdoutDest = metadataDir + "/stdout";
    const std::string stderrDest = metadataDir + "/stderr";
    const std::string exitCodeDest = metadataDir + "/exit_code";

    if (actionResult.stdout_raw().size()) {
        buildboxcommon::FileUtils::writeFileAtomically(
            stdoutDest, std::string(actionResult.stdout_raw()));
        std::cout << "Wrote inline stdout to: " << metadataDir << std::endl;
    }
    else if (actionResult.stdout_digest() != Digest()) {
        blobs.emplace(
            actionResult.stdout_digest().hash(),
            buildboxcommon::Client::OutputMap::mapped_type(stdoutDest, false));

        digests.push_back(actionResult.stdout_digest());
    }
    if (actionResult.stderr_raw().size()) {
        buildboxcommon::FileUtils::writeFileAtomically(
            stderrDest, actionResult.stderr_raw());
        std::cout << "Wrote inline stderr to: " << stderrDest << std::endl;
    }
    else if (actionResult.stderr_digest() != Digest()) {
        blobs.emplace(
            actionResult.stderr_digest().hash(),
            buildboxcommon::Client::OutputMap::mapped_type(stderrDest, false));

        digests.push_back(actionResult.stderr_digest());
    }

    writeExitCode(exitCodeDest, actionResult.exit_code());
    // TODO: output_symlinks
    remoteCasClient->downloadBlobs(digests, blobs);
}
bool digestFromString(const std::string &s, Digest *digest)
{
    // "[hash in hex notation]/[size_bytes]"
    static const std::regex regex("([0-9a-fA-F]+)/(\\d+)");
    std::smatch matches;
    if (std::regex_search(s, matches, regex) && matches.size() == 3) {
        const std::string hash = matches[1];
        const std::string size = matches[2];

        digest->set_hash(hash);
        digest->set_size_bytes(std::stoll(size));
        return true;
    }

    return false;
}

int runCasDownload(int argc, char *argv[])
{
    // Parsing arguments:
    if (argc < 5) {
        std::cerr << "Error: missing arguments" << std::endl;
        printUsage(argv[0]);
        return 1;
    }

    enum DigestType { DIGEST_ROOT, DIGEST_ACTION, DIGEST_FILE };

    DigestType digestType = DIGEST_ROOT;
    std::string casServerAddress, downloadDirectory, instance, digest;
    for (int arg = 1; arg < argc; arg++) {
        const std::string parm(argv[arg]);
        if (parm.find("--instance=") == 0) {
            instance = parm.substr(strlen("--instance="));
        }
        else if (parm.find("--cas-server=") == 0) {
            casServerAddress = parm.substr(strlen("--cas-server="));
        }
        else if (parm.find("--root-digest=") == 0) {
            digest = parm.substr(strlen("--root-digest="));
        }
        else if (parm.find("--action-digest=") == 0) {
            digest = parm.substr(strlen("--action-digest="));
            digestType = DIGEST_ACTION;
        }
        else if (parm.find("--file-digest=") == 0) {
            digest = parm.substr(strlen("--file-digest"));
            digestType = DIGEST_FILE;
        }
        else if (parm.find("--destination-dir=") == 0) {
            downloadDirectory = parm.substr(strlen("--destination-dir="));
        }
        else {
            std::cerr << "Unrecognized argument: " << parm << std::endl;
            printUsage(argv[0]);
            return 1;
        }
    }
    buildboxcommon::ConnectionOptions connectionOptions;
    connectionOptions.d_url = casServerAddress.c_str();
    connectionOptions.d_instanceName = instance.c_str();

    // connect to remote CAS server
    std::cout << "CAS client connecting to " << casServerAddress << std::endl;

    buildboxcommon::Client remoteCasClient;
    remoteCasClient.init(connectionOptions);

    Digest protoDigest;
    if (!digestFromString(digest, &protoDigest)) {
        std::cerr << "Error: invalid digest \"" << digest << "\"" << std::endl;
        printUsage(argv[0]);
        return 1;
    }

    if (!buildboxcommon::FileUtils::isDirectory(downloadDirectory.c_str())) {
        if (buildboxcommon::FileUtils::isRegularFile(
                downloadDirectory.c_str())) {
            std::cerr
                << "Error: [" << downloadDirectory << "] was specified "
                << "as destination download directory but already exists "
                << "as file.\n"
                << std::endl;
            printUsage(argv[0]);
            return 1;
        }
        const std::string d = calculateDirname(downloadDirectory);
        if (buildboxcommon::FileUtils::isRegularFile(d.c_str())) {
            std::cerr << "Error: [" << downloadDirectory << "] was specified "
                      << "as destination download directory but " << d
                      << " is a file.\n"
                      << std::endl;

            return 1;
        }
    }

    buildboxcommon::FileUtils::createDirectory(downloadDirectory.c_str());
    std::cout << "Starting to download " << protoDigest << " to \""
              << downloadDirectory << "\"" << std::endl;

    auto start = std::chrono::high_resolution_clock::now();

    if (digestType == DIGEST_ACTION) {
        buildboxcommon::ActionResult actionResult;
        if (!actionResultFromAction(protoDigest, connectionOptions,
                                    &remoteCasClient, &actionResult)) {
            std::cerr << "Failed to download action result with digest "
                      << digest << std::endl;
            return 1;
        }
        const std::string metadataDir =
            downloadDirectory + "/" + protoDigest.hash() + "-out";
        buildboxcommon::FileUtils::createDirectory(metadataDir.c_str());
        writeActionResultToDisk(actionResult, downloadDirectory, metadataDir,
                                &remoteCasClient);
    }
    else if (digestType == DIGEST_ROOT) {
        remoteCasClient.downloadDirectory(protoDigest, downloadDirectory);
    }
    else {
        buildboxcommon::Client::DownloadedData res =
            remoteCasClient.downloadBlobs({protoDigest});
        if (!res.empty()) {
            buildboxcommon::FileUtils::writeFileAtomically(
                downloadDirectory + "/blob", res[protoDigest.hash()]);
            std::cout << "Downloaded blob to " << downloadDirectory + "/blob"
                      << std::endl;
        }
        else {
            std::cerr << "Could not download blob with digest " << protoDigest
                      << std::endl;
            return 1;
        }
    }

    auto end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsed = end - start;

    std::cout << "Finished downloading " << protoDigest << " to \""
              << downloadDirectory << "\" in " << std::fixed
              << std::setprecision(3) << elapsed.count() << " second(s)"
              << std::endl;
    return 0;
}

int main(int argc, char *argv[])
{
    try {
        return runCasDownload(argc, argv);
    }
    catch (const std::exception &e) {
        std::cerr << "ERROR: Caught exception [" << e.what()
                  << "] while running " << argv[0] << std::endl;
        return -1;
    }
}
