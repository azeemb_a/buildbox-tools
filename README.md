# buildbox-tools

Command-line utilities for BuildBox, BuildGrid, and other Remote Execution
services.

Please refer to each individual tool for usage and build instructions.

## casdownload

`casdownload` facilitates downloading blobs, action outputs and directory trees
from a previously configured Content Addressable Store and Action Cache (such
as BuildGrid, Buildbarn or Buildfarm).

Please refer to the [README.md](cpp/casdownload/README.md) for more information.

## chrootbuilder

`chrootbuilder` generates chroot images (filesystem tarballs)
from a docker container or image. It can also make the chroot image compatible with
`buildbox-run-userchroot`.

Please refer to the [README.md](python/chrootbuilder/README.md) for more information.
